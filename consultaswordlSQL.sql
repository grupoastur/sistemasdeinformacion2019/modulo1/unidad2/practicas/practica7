﻿
USE world;

-- 1 Listar todos los lenguajes oficiales de España (España es el localname)
 SELECT DISTINCT * FROM country c WHERE c.LocalName='españa'; 
 SELECT c.Language FROM countrylanguage c JOIN ( SELECT DISTINCT * FROM country c WHERE c.LocalName='españa')c1 ON c.CountryCode= c1.Code;  

-- 2 Listar todos los lenguajes de España
SELECT DISTINCT c1.Language FROM country c JOIN countrylanguage c1 ON c.Code = c1.CountryCode WHERE c.LocalName='españa';

-- 3 Indicar la población media por continente y región (ordenándolo por ambos campos) 
  SELECT AVG( c.Population) FROM country c GROUP BY c.Continent;

-- 4 Indicar el nombre de todas las ciudades del continente cuya población media es la mayor
  -- subconsulta
  SELECT AVG( c.Population) media_ciudad, c1.Continent, c.Name FROM city c JOIN country c1 ON c.CountryCode = c1.Code GROUP BY c.Name ;
 -- consulta final
SELECT MAX( c1.media_ciudad), c1.Continent FROM 
(SELECT AVG( c.Population) media_ciudad, c1.Continent, c.Name FROM city c JOIN country c1 ON c.CountryCode = c1.Code GROUP BY c.Name) c1 GROUP BY c1.Continent;

-- 5 Indicar la población media de las ciudades del continente cuyos países tengan la esperanza de vida media mayor
SELECT c.LifeExpectancy media, c.Name FROM country c GROUP BY c.Code;
   SELECT MAX( c1.media) FROM (SELECT AVG(c.LifeExpectancy) media, c.Name FROM country c GROUP BY c.Code) c1;
  
-- 6 Indicar la población y el nombre de las capitales de cada uno de los países. Además, debemos indicar el país del cual es capital
-- y el nombre del continente al que pertenece
  SELECT c.Name capital, c.Population poblacion_de_la_capital, country.Name pais, Continent FROM country
  JOIN city c ON country.Code = c.CountryCode GROUP BY Capital;
  
-- 7 Indicar el lenguaje en cada país cuyo porcentaje sea el mayor (porcentaje)
  SELECT c.Language, c1.Name, c.Percentage FROM countrylanguage c JOIN country c1 ON c.CountryCode = c1.Code GROUP BY c1.Code; 
  
-- 8 Indicar los continentes que mayor número de lenguas se hablen
  SELECT COUNT(*) FROM countrylanguage c GROUP BY c.Language ;
SELECT MAX( c1.numero) maximo FROM ( SELECT COUNT(*) numero FROM countrylanguage c GROUP BY c.Language) c1;                 
SELECT c.Continent FROM (SELECT MAX( c1.numero) maximo FROM ( SELECT COUNT(*) numero FROM countrylanguage c GROUP BY c.Language) c1) c2; 

-- 9 Indicar los países que menor número de lenguas se hablen
  SELECT c1.Language, c.LocalName paises FROM country c JOIN countrylanguage c1 ON c.Code = c1.CountryCode GROUP BY c1.Language;
  SELECT MIN( c1.Language) numero, c1.Name FROM( SELECT c1.Language, c.Name FROM country c JOIN countrylanguage c1 ON c.Code = c1.CountryCode GROUP BY c.Name) c1; 

-- 10 Indicar los países que tengan el mismo número de lenguas que España
 SELECT COUNT(*) FROM countrylanguage c JOIN ( SELECT DISTINCT * FROM country c WHERE c.LocalName='españa')c1 ON c.CountryCode= c1.Code;  
   